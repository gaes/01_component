# What is AngularJS? #
AngularJS is a structural framework for dynamic web apps power by googl
It lets you use HTML as your template language and lets you extend HTML's syntax to express your application's 
components clearly and succinctly. 
Angular's data binding and dependency injection eliminate much of the code you currently have to write. 


- AngularJS provides developers options to write client side application (using JavaScript) in a clean MVC(Model View Controller) way.

- Application written in AngularJS is cross-browser compliant. AngularJS automatically handles JavaScript code suitable for each browser.

- AngularJS is open source, completely free, and used by thousands of developers around the world.

# install Angular 4 #
npm install -g @angular/cli

ng -v

# new project#
ng new MyApp

# new component#
https://stackoverflow.com/questions/44151427/how-to-create-a-new-component-in-angular-4-using-cli

ng g component --spec=false namecomp 

# architecture #
https://angular.io/guide/architecture

# VS plugin#
Install Debugger for chrome
installar launch.json